package mmda.sql;
import java.sql.Types;

public enum Attribute {
	//GUID varchar(36) ,TYPE varchar(10) ,NAME varchar(256), KEYWORDS varchar(256), 
	//ACCESSDATE TIMESTAMP, CREATEDATE TIMESTAMP, MODIFIEDDATE TIMESTAMP, SIZE int, AUTHOR varchar(63), PRIMARY KEY(GUID)
	PARENTGUID("PARENTGUID",Types.VARCHAR),
	CHILDGUID("CHILDGUID",Types.VARCHAR),
	GUID("GUID",Types.VARCHAR),
	PATH("PATH",Types.VARCHAR),
	TYPE("TYPE",Types.VARCHAR),
	NAME("NAME",Types.VARCHAR),
	KEYWORD("KEYWORDS",Types.VARCHAR),
	DATEACCESSED("ACCESSDATE",Types.TIMESTAMP), 
	DATECREATED("CREATEDATE",Types.TIMESTAMP),
	DATEMODIFIED("MODIFIEDDATE",Types.TIMESTAMP),
	SIZE("BYTE",Types.BIGINT),
	AUTHOR("AUTHOR",Types.VARCHAR);
	String name;
	int type;
	Attribute(String name, int type){
		this.name = name;
		this.type = type;
	}
}

