package mmda.sql;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.sql.Types;

import common.util.FileUtility;

import oracle.jdbc.pool.OracleDataSource;

public class SQLQueryBuilder {
	private StringBuilder builder = new StringBuilder();
	private String tableNameQuery="SELECT table_name FROM USER_TABLES";
	private String username = "dbclass151",password="123456";
	private TreeMap<String,String> tableQueries=null;
	private OracleDataSource source=null;
	private Connection conn=null;
	private Statement statement = null;
	private PreparedStatement insertDARG,updateDARG,deleteDARG,getDARG,insertCD,deleteCD,getParent,getChildren,insertMT,deleteMT,orphan,sterile,createAfter,createBefore,createBetween,getMetadata,getPath,keywordSearch;
	private PreparedStatement searchDARG, getAll;
	private TreeSet<String> referencedTableNames = new TreeSet<String>();
	private String update = "UPDATE ",whitespace = " ",equal = " = ", set = " SET ",where = " WHERE ",select = "SELECT ", from = " FROM ", and  = " AND ",question = " ? ",comma = " , ",dot = ".";
	private TreeSet<Attribute> metadataAttributes = new TreeSet<Attribute>(); 
	private TreeMap<Attribute,String> prefixs = new TreeMap<Attribute,String>();
	public SQLQueryBuilder(){
		StartConnection();
		referencedTableNames.add("DARG");
		tableQueries = new TreeMap<String,String> ();
		tableQueries.put("DARG", "CREATE TABLE DARG (GUID varchar(36), PATH varchar(256),PRIMARY KEY(GUID))");
		tableQueries.put("CHILDREN", "CREATE TABLE CHILDREN (PARENTGUID varchar(36) ,CHILDGUID varchar(36), PRIMARY KEY(PARENTGUID,CHILDGUID),CONSTRAINT FK_CD FOREIGN KEY(PARENTGUID) REFERENCES DARG(GUID) ON DELETE CASCADE)");
		tableQueries.put("METADATA", "CREATE TABLE METADATA (GUID varchar(36) ,TYPE varchar(25) ,NAME varchar(256), KEYWORDS varchar(256), ACCESSDATE TIMESTAMP, CREATEDATE TIMESTAMP, MODIFIEDDATE TIMESTAMP, BYTE NUMBER(19), AUTHOR varchar(63), PRIMARY KEY(GUID), CONSTRAINT FK_CH FOREIGN KEY(GUID) REFERENCES DARG(GUID) ON DELETE CASCADE)");
		
		initializeTables(tableQueries);
		metadataAttributes.add(Attribute.GUID);
		metadataAttributes.add(Attribute.TYPE);
		metadataAttributes.add(Attribute.NAME);
		metadataAttributes.add(Attribute.KEYWORD);
		metadataAttributes.add(Attribute.DATEACCESSED);
		metadataAttributes.add(Attribute.DATECREATED);
		metadataAttributes.add(Attribute.DATEMODIFIED);
		metadataAttributes.add(Attribute.SIZE);
		metadataAttributes.add(Attribute.AUTHOR);
		try {
			insertDARG = conn.prepareStatement("INSERT INTO DARG VALUES (?,?)");
			updateDARG = conn.prepareStatement("UPDATE DARG SET PATH = ? WHERE GUID = ?");
			deleteDARG = conn.prepareStatement("DELETE FROM DARG WHERE GUID = ?");
			getDARG = conn.prepareStatement("SELECT D.GUID FROM DARG D WHERE D.PATH = ?");
			insertCD = conn.prepareStatement("INSERT INTO CHILDREN VALUES (?,?)");			
			deleteCD = conn.prepareStatement("DELETE FROM CHILDREN WHERE PARENTGUID = ? and CHILDGUID = ?");
			getParent = conn.prepareStatement("SELECT CD.PARENTGUID FROM CHILDREN CD WHERE CHILDGUID = ?");
			getChildren = conn.prepareStatement("SELECT CD.CHILDGUID FROM CHILDREN CD WHERE PARENTGUID = ?");
			insertMT = conn.prepareStatement("INSERT INTO METADATA VALUES (?,?,?,?,?,?,?,?,?)");
			deleteMT = conn.prepareStatement("DELETE FROM METADATA WHERE GUID = ?");
			orphan = conn.prepareStatement("select distinct D.GUID from DARG D MINUS select c.parentguid FROM CHILDREN C");
			sterile = conn.prepareStatement("select distinct D.GUID from DARG D MINUS select c.childguid FROM CHILDREN C");
			createBefore = conn.prepareStatement("select M.GUID from METADAT M WHERE M.CREATEDATE <= ?");
			createAfter = conn.prepareStatement("select M.GUID from METADAT M WHERE M.CREATEDATE  >= ?");
			createBefore = conn.prepareStatement("select M.GUID from METADAT M WHERE M.CREATEDATE >= ? AND M.CREATEDATE <= ? ");
			getMetadata = conn.prepareCall("SELECT * FROM METADATA M WHERE M.GUID = ?");
			getPath = conn.prepareStatement("SELECT D.PATH FROM DARG D WHERE D.GUID = ?");
			keywordSearch = conn.prepareStatement("Select M.GUID FROM METADATA M WHERE M.KEYWORDS LIKE ?");
			getAll = conn.prepareStatement("SELECT D.GUID FROM DARG D");
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
	}
	public void StartConnection(){
		try {
			source = new OracleDataSource();
			source.setDataSourceName("Oracle");
		    source.setDriverType("thin");
		    source.setDatabaseName("dbclass1");
		    source.setPortNumber(1521);
		    source.setServerName("ginger.umd.edu");
		    source.setUser(username);
		    source.setPassword(password);
		    source.setNetworkProtocol("tcp");
		    conn = source.getConnection();
		    statement = conn.createStatement();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	public void closeConnectin(){
		if(statement==null||conn==null){
			return;
		}
		try {
			statement.close();
			conn.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	public void initializeTables(TreeMap<String,String> tableQueries){
		TreeSet<String> tableNames = new TreeSet<String>();
		String str = null ;
		try {
			PreparedStatement existStatement = conn.prepareStatement(tableNameQuery);
			if(existStatement.execute()){
				ResultSet result = existStatement.getResultSet();
				while(result.next()){
					tableNames.add(result.getString(1));
				}
				for(String tableName:referencedTableNames){
					if(!tableNames.contains(tableName)){
						statement.execute(tableQueries.get(tableName));
						tableNames.add(tableName);
					}
				}
				for(Entry<String,String> tableQuery:tableQueries.entrySet()){
					if(!tableNames.contains(tableQuery.getKey())){
						str = tableQuery.getValue();
						statement.execute(str);
						
					}
				}
			}
			
		} catch (SQLException e) {
			System.out.println(str);
			e.printStackTrace();
			
		}

	}
	public void dropAllTables(){
		TreeSet<String> tableNames = new TreeSet<String>();
		String str = null;
		try {
			PreparedStatement existStatement = conn.prepareStatement(tableNameQuery);
			if(existStatement.execute()){
				ResultSet result = existStatement.getResultSet();
				while(result.next()){
					tableNames.add(result.getString(1));
				}

			}
			
		
			String dropQuery = "DROP TABLE ";
			//tableNames.removeAll(referencedTableNames);
			for(String tableName: tableNames){
				if(!referencedTableNames.contains(tableName)){
				builder.setLength(0);
				builder.append(dropQuery);
				builder.append(tableName);
				statement.execute(builder.toString());
				}
			}
			for(String tableName: referencedTableNames){			
				if(tableNames.contains(tableName)){
					builder.setLength(0);
					builder.append(dropQuery);
					builder.append(tableName);
					str = builder.toString();
					statement.execute(str);
				}
			}
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
	}
	public boolean insertDARG(String path,TreeMap<Attribute,Object> metadata, String parentGUID){
		boolean duplicate = false;
		
		if(!FileUtility.isFileExist(path)){
			return false;
		}
		if(FileUtility.isUrl(path)){
			return insertExternalDARG(path,metadata);
		}
		File file = new File(path);
		if(file.isDirectory()){
			for(File f:file.listFiles()){
				metadata.clear();
				addMetadata(metadata,f.toPath().toString());
				insertDARGToDatabase(path,metadata,null);
			}
			return true;
		}else{
		addMetadata(metadata,path);
		return insertDARGToDatabase(path,metadata,parentGUID);}
		
			
	}
	public boolean insertDARGToDatabase(String path,TreeMap<Attribute,Object> metadata, String parentGUID){
		boolean duplicate = false;
		duplicate = isDuplicate(path,metadata);
		String GUID = UUID.randomUUID().toString();
		try {
			duplicate = isDuplicate(path,metadata);
			if(!duplicate){
				insertDARG.setString(1, GUID);
				insertDARG.setString(2, path);
				insertDARG.executeUpdate();		
				insertMT(GUID,metadata);
			}else{
				GUID = getDARG(path);
				
			}
			if(parentGUID != null){
				TreeSet<String> guid = new TreeSet<String>();
				guid.add(GUID);
				TreeSet<String> parent = new TreeSet<String>();
				getParentGUID(guid,1,parent);
				if(!parent.contains(parentGUID)){
					insertCD(parentGUID,GUID);
				}
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean insertExternalDARG(String path,TreeMap<Attribute,Object> metadata){
		String GUID = getDARG(path);
		if(GUID==null){
			GUID = UUID.randomUUID().toString();
		}else{
			return false;
		}
		TreeSet<TreeMap<Attribute,Object>> children = FileUtility.getExternalHTML(path, metadata);
		
		try {
			insertDARG.setString(1, GUID);
			insertDARG.setString(2, path);
			insertDARG.executeUpdate();	
			for(TreeMap<Attribute,Object> meta: children){
				insertDARGToDatabase((String)meta.get(Attribute.PATH),meta,GUID);
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;	
	}
	public String getDARG(String path){
		try {
			getDARG.setString(1, path);
			getDARG.execute();
			ResultSet r = getDARG.getResultSet();
			if(r.next()){
				return r.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return null;
	}
	public boolean isDuplicate(String path,TreeMap<Attribute,Object> metadata){
		if(getDARG(path)!=null){
			return true;
		}else{
			TreeMap<Attribute,Object> meta = new TreeMap<Attribute,Object>();
			
			meta.put(Attribute.SIZE, metadata.get(Attribute.SIZE));
			
			TreeSet<TreeMap<Attribute,Object>> matches = searchDAGR(meta);
			if(matches.isEmpty()){
				return false;
			}else{
				for(TreeMap<Attribute,Object> m: matches){
					if(FileUtility.areFileEquals((String)m.get(Attribute.PATH),path)){
						return true;
					}
				}
				return false;
			}
		}
	}
	public void updateDAGR(String GUID, TreeMap<Attribute,Object> metadata){
		
		try {
			if(metadata.containsKey(Attribute.PATH)){
			updateDARG.setString(1, (String)metadata.get(Attribute.PATH));
			updateDARG.setString(2, GUID);
			updateDARG.executeUpdate();
			}
			updateMT(GUID,metadata);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	
	}
	public void deleteDARG(String GUID,String parentGUID){
		try {
			if(getPath(GUID)==null){
				return;
			}
			TreeSet<String> curr = new TreeSet<String>();
			curr.add(GUID);
			TreeSet<String> parent = new TreeSet<String>();
			getParentGUID(curr,1,parent);			
			for(String pGuid: parent){
				deleteCD(pGuid,GUID);
			}	
				
			TreeSet<String> children = new TreeSet<String>();
			getChildrenGUID(curr,1,children);				
			deleteDARG.setString(1, GUID);
			deleteDARG.executeUpdate();				
	
			for(String str:children){
				deleteDARG.setString(1, str);
				deleteDARG.executeUpdate();		
				deleteCD(GUID,str);
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	public void insertCD(String parentGUID, String childGUID){
		try {
			insertCD.setString(1, parentGUID);
			insertCD.setString(2, childGUID);
			insertCD.executeUpdate();
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
	}
	public void deleteCD(String parentGUID, String childGUID){
		try {
			deleteCD.setString(1, parentGUID);
			deleteCD.setString(2, childGUID);
			deleteCD.executeUpdate();
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
	}
	public TreeSet<String> getParentGUID(TreeSet<String> GUIDs, int level, TreeSet<String> parents){
		TreeSet<String> currGUIDs = new TreeSet<String>();
		if(level == 0){
			parents.addAll(GUIDs);
			return parents;
		}else{
			try {
				for(String GUID: GUIDs){				
					getParent.setString(1, GUID);
					getParent.execute();
					ResultSet result = getParent.getResultSet();
					while(result.next()){
						currGUIDs.add(result.getString(1));
					}
				}
				parents.addAll(GUIDs);
				return getParentGUID(currGUIDs,level-1,parents);
				
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
				
				
			
		}
	}
	public TreeSet<String> getChildrenGUID(TreeSet<String> GUIDs, int level, TreeSet<String> children){
		TreeSet<String> currGUIDs = new TreeSet<String>();
		if(level == 0){
			children.addAll(GUIDs);
			return children;
		}else{
			try {
				for(String GUID: GUIDs){				
					getChildren.setString(1, GUID);
					getChildren.execute();
					ResultSet result = getChildren.getResultSet();
					while(result.next()){
						currGUIDs.add(result.getString(1));
					}
				}
				children.addAll(GUIDs);
				return getChildrenGUID(currGUIDs,level-1,children);
				
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
				
				
			
		}
	}
	public void insertMT(String GUID, TreeMap<Attribute,Object> metadata){
		try {
			metadata.put(Attribute.GUID, GUID);
			setStatement(insertMT,metadata,0,metadataAttributes);
			insertMT.execute();		
		} catch (SQLException e) {	
			e.printStackTrace();
		}
	}
	public void updateMT(String GUID, TreeMap<Attribute,Object> metadata){
		TreeMap<Attribute,Object> keys = new TreeMap<Attribute,Object>();
		keys.put(Attribute.GUID, GUID);
		PreparedStatement statement = createUpdateQuery("METADATA",metadata,builder,keys);
		setStatement(statement,metadata,0);
		setStatement(statement,keys,metadata.size());
		try {
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void deleteMT(String GUID){
		try {		
			deleteMT.setString(1, GUID);
			deleteMT.executeUpdate();
		} catch (SQLException e) {	
			e.printStackTrace();
		}
	}
	public TreeSet<String> getKeywordSearch(String keyword){
		String str = keyword;
		TreeSet<String> guid = new TreeSet<String> ();
		if(!keyword.contains("%")){
			str = "%"+keyword+"%";
		}
		try {
			keywordSearch.setString(1, str);
			keywordSearch.execute();
			ResultSet result = keywordSearch.getResultSet();
			while(result.next()){
				guid.add(result.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return guid;
	}
	public TreeSet<TreeMap<Attribute,Object>> searchDAGR(TreeMap<Attribute,Object> attributes){
		TreeSet<String> childrenQuery = new TreeSet<String>(),curr = new TreeSet<String>(),children = null,parent = null,keyword = null;;
		TreeSet<TreeMap<Attribute,Object>> metas = new TreeSet<TreeMap<Attribute,Object>>(FileUtility.treemapComp);
		TreeSet<String> darg = new TreeSet<String>();
		if(attributes.containsKey(Attribute.PARENTGUID)){
			children = new TreeSet<String>();
			curr.add((String)attributes.get(Attribute.PARENTGUID));
			getChildrenGUID(curr,1,children);
			attributes.remove(Attribute.PARENTGUID);
		}
		if(attributes.containsKey(Attribute.CHILDGUID)){
			parent=new TreeSet<String>();
			curr.clear();
			curr.add((String)attributes.get(Attribute.CHILDGUID));
			getParentGUID(curr,1,parent);
			attributes.remove(Attribute.CHILDGUID);
			
		}
		if(attributes.containsKey(Attribute.KEYWORD)){
			curr.clear();
			keyword=getKeywordSearch((String)attributes.get(Attribute.KEYWORD));
			attributes.remove(Attribute.KEYWORD);
			
			
		}
		if(attributes.isEmpty()){
				
				if(children!=null){
				darg.addAll(children);
				}
				if(parent!=null){
					darg.addAll(parent);
				}
				if(keyword!=null){
					darg.addAll(keyword);
				}
				if(children!=null){
					darg.retainAll(children);
					}
					if(parent!=null){
				darg.retainAll(parent);
				}
				if(keyword!=null){
				darg.retainAll(keyword);
				}
				
			for(String str:darg){
				metas.add(getMetadata(str));
			}
			return metas;
		}
		if(attributes.containsKey(Attribute.PATH)){
			metas.add(getMetadata(getDARG((String)attributes.get(Attribute.PATH))));
			return metas;
		}
		builder.setLength(0);
		builder.append(select);
		builder.append(Attribute.GUID.name);
		builder.append(from);
		builder.append("METADATA MT");
		builder.append(where);
		addAttributeToQuery(builder,attributes,and," MT.");
		
		
		try {
			
			PreparedStatement statement = conn.prepareStatement(builder.toString());
			setStatement(statement,attributes,0);
			statement.execute();
			ResultSet result = statement.getResultSet();
			if(result.next()){
				darg.add(result.getString(1));
				
			}
			if(children!=null){
			darg.retainAll(children);
			}
			if(parent!=null){
			darg.retainAll(parent);
			}
			if(keyword!=null){
			darg.retainAll(keyword);
			}
			for(String guid:darg){
				metas.add(getMetadata(guid));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(builder.toString());
		}
		
		return metas;
	}
	
	public TreeMap<Attribute,Object> getMetadata(String GUID){
		TreeMap<Attribute,Object> meta = new TreeMap<Attribute,Object>();
		try {
			getMetadata.setString(1, GUID);
			if(getMetadata.execute()){
				ResultSet result = getMetadata.getResultSet();
				if(result.next()){
					for(Attribute attr: metadataAttributes){					
						meta.put(attr, result.getObject(attr.name));
					}
				}
			}
			meta.put(Attribute.PATH, getPath(GUID));
			meta.put(Attribute.GUID, GUID);
			return meta;
		} catch (SQLException e) {
			e.printStackTrace();
			return meta;
		}
		
	}
	public String getPath(String GUID){
		try {
			getPath.setString(1, GUID);
			if(getPath.execute()){
				ResultSet result = getPath.getResultSet();
				if(result.next()){									
					return result.getString(Attribute.PATH.name);				
				}
			}
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	public TreeSet<TreeMap<Attribute,Object>> getOrphanReport(){
		TreeSet<TreeMap<Attribute,Object>> metas = new TreeSet<TreeMap<Attribute,Object>>(FileUtility.treemapComp);
		try {
			if(orphan.execute()){
				ResultSet result = orphan.getResultSet();
				while(result.next()){
				
					metas.add(getMetadata(result.getString(1)));
				}
			}
			return  metas;
		} catch (SQLException e) {
			e.printStackTrace();
			return  metas;
		}
	}
	public TreeSet<TreeMap<Attribute,Object>> getSterileReport(){
		TreeSet<TreeMap<Attribute,Object>> metas = new TreeSet<TreeMap<Attribute,Object>>(FileUtility.treemapComp);
		try {
			if(sterile.execute()){
				ResultSet result = sterile.getResultSet();
				while(result.next()){
				
					metas.add(getMetadata(result.getString(1)));
				}
			}
			return metas;
		} catch (SQLException e) {
			e.printStackTrace();
			return metas;
		}
	}
	public TreeSet<TreeMap<Attribute,Object>> getTimeRangeReport(Calendar from, Calendar to){
		TreeSet<TreeMap<Attribute,Object>> metas = new TreeSet<TreeMap<Attribute,Object>>(FileUtility.treemapComp);
		
		TreeSet<String> inRange = new TreeSet<String>();
		try {
			if(from==null && to == null){
				
			}else if(from == null){
				createBefore.setTimestamp(1,new Timestamp(to.getTimeInMillis()));
				if(createBefore.execute()){
					ResultSet result = createBefore.getResultSet();
					while(result.next()){
						inRange.add(result.getString(1));
					}
				}
			}else if(to == null){
				createAfter.setTimestamp(1,new Timestamp(from.getTimeInMillis()));
				if(createAfter.execute()){
					ResultSet result = createAfter.getResultSet();
					while(result.next()){
						inRange.add(result.getString(1));
					}
				}	
			}else{
				createBetween.setTimestamp(1,new Timestamp(from.getTimeInMillis()));
				createBetween.setTimestamp(2,new Timestamp(to.getTimeInMillis()));
				if(createBetween.execute()){
					ResultSet result = createBetween.getResultSet();
					while(result.next()){
						inRange.add(result.getString(1));
					}
				}	
			}
			for(String str:inRange){
				metas.add(getMetadata(str));
			}
			return metas;
		} catch (SQLException e) {
			e.printStackTrace();
			return metas;
		}
	}
	private PreparedStatement createUpdateQuery(String tableName, TreeMap<Attribute,Object> metadata,StringBuilder builder, TreeMap<Attribute,Object> primaryKeys){
		builder.append(update);
		builder.append(tableName);
		builder.append(set);
		addAttributeToQuery(builder,metadata,comma,null);
		builder.append(where);
		addAttributeToQuery(builder,primaryKeys,and,null);
		try {
		
			return conn.prepareStatement(builder.toString());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	private void setStatement (PreparedStatement statement, TreeMap<Attribute,Object> metadata,int offset){
			int i = 1 + offset;
			
				try{
					for(Entry<Attribute,Object> entry:metadata.entrySet()){		
						if(entry.getValue()==null){
							statement.setNull(i, entry.getKey().type);
						}else{
							switch(entry.getKey().type){
							case Types.VARCHAR:							
								statement.setString(i,(String)entry.getValue());
								break;
							case Types.BIGINT:
								statement.setLong(i, (Long)entry.getValue());
								break;
							case Types.TIMESTAMP:
								statement.setTimestamp(i, (Timestamp)entry.getValue());
								break;
							}
						}
						i++;
					}
				}catch(SQLException e){
					e.printStackTrace();
					System.err.println("SQL EXCEPTION");
				
				}				
			
	}
	private void setStatement (PreparedStatement statement, TreeMap<Attribute,Object> metadata,int offset, TreeSet<Attribute> requiredAttributes){
		int i = 1 + offset;
		Object o = null;
			try{
				for(Attribute attr:requiredAttributes){				
					switch(attr.type){
						case Types.VARCHAR:
							if((o = metadata.get(attr))!=null){
								statement.setString(i,(String)o);
							}else{
								statement.setNull(i, attr.type);								
							}
							break;
						case Types.BIGINT:
							if((o = metadata.get(attr))!=null){
								statement.setLong(i, (Long)o );
							}else{
								statement.setNull(i, attr.type);
							}
							break;
						case Types.TIMESTAMP:
							if((o = metadata.get(attr))!=null){
								statement.setTimestamp(i, (Timestamp)o);
							}else{
								statement.setNull(i, attr.type);
							}
							break;
					}
					i++;
				}
			}catch(SQLException e){
				e.printStackTrace();
				System.err.println("SQL EXCEPTION");
			
			}				
		
}
	private void addAttributeToQuery(StringBuilder builder,TreeMap<Attribute,Object> attributes,String seperator,String prefix){
		int i = 1, size = attributes.size();
		for(Entry<Attribute,Object> entry:attributes.entrySet()){
			if(prefix!=null){
				builder.append(prefix);
			}
			builder.append(entry.getKey().name);
			builder.append(equal);
			builder.append(question);
			if(i!=size){
			builder.append(seperator);
			}
			i ++ ;
		}
	}
	private void addMetadata(TreeMap<Attribute,Object> meta,String path){
		if(FileUtility.isFileExist(path)){
			try {
				File file = new File(path);
				BasicFileAttributes basicFileAttributes = Files.getFileAttributeView(file.toPath(), BasicFileAttributeView.class).readAttributes();
				meta.put(Attribute.SIZE, new Long(basicFileAttributes.size()));
				meta.put(Attribute.DATECREATED, new Timestamp(basicFileAttributes.creationTime().toMillis()));
				meta.put(Attribute.DATEACCESSED, new Timestamp(basicFileAttributes.lastAccessTime().toMillis()));
				meta.put(Attribute.DATEMODIFIED, new Timestamp(basicFileAttributes.lastModifiedTime().toMillis()));
				meta.put(Attribute.NAME, file.getName());
				String[] split = path.split("[.]");
				if(split.length>1){
					meta.put(Attribute.TYPE, split[split.length-1]);
				}else{
					
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public void modifyAllDARG(TreeSet<TreeMap<Attribute,Object>> metas){	
		//metas.retainAll(getOrphanReport());
		for(TreeMap<Attribute,Object> tree:metas){
			deleteDARG((String)tree.get(Attribute.GUID),null);
			insertDARG((String)tree.get(Attribute.PATH),tree,null);
		}
	}
	public TreeSet<TreeMap<Attribute,Object>> getAll(){
		TreeSet<TreeMap<Attribute,Object>> metas = new TreeSet<TreeMap<Attribute,Object>>();;
		try {
			getAll.execute();
			ResultSet result = getAll.getResultSet();
			while(result.next()){
				metas.add(getMetadata(result.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		return metas;
	}
	
	
}
