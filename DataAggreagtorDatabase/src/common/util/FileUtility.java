package common.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.TreeSet;

import mmda.sql.Attribute;
import mmda.sql.SQLQueryBuilder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

public class FileUtility {
	public static Comparator<TreeMap<Attribute,Object>> treemapComp = new Comparator<TreeMap<Attribute,Object>>(){

		@Override
		public int compare(TreeMap<Attribute, Object> arg0,
				TreeMap<Attribute, Object> arg1) {
			return arg0.hashCode()-arg1.hashCode();
		}
		
	};
	public static long getCreationDate(String path){
		try {
			BasicFileAttributes basicFileAttributes = Files.getFileAttributeView(new File(path).toPath(), BasicFileAttributeView.class).readAttributes();
			return basicFileAttributes.creationTime().toMillis();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;		
	}
	public static long getLastModifiedTime(String path){
		try {
			BasicFileAttributes basicFileAttributes = Files.getFileAttributeView(new File(path).toPath(), BasicFileAttributeView.class).readAttributes();
			return basicFileAttributes.lastModifiedTime().toMillis();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;		
	}
	public static long getLastAccessTime(String path){
		try {
			BasicFileAttributes basicFileAttributes = Files.getFileAttributeView(new File(path).toPath(), BasicFileAttributeView.class).readAttributes();
			return basicFileAttributes.lastAccessTime().toMillis();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;		
	}
	public static boolean isUrl(String path){
		return path.contains("http://")||path.contains("www.");
	}
	public static boolean isFileExist(String path){
		if(path.contains("http")||path.contains("www")){
			return Jsoup.connect(path)!=null;
		}
		File file = new File(path);
		return file.exists();
	}
	public static void HtmlParaser(String path){
		Document doc = null;
		String parentPath = null;
		try {
			if(path.contains("http")||path.contains("www")){
				doc = Jsoup.connect(path).get();
				parentPath = doc.baseUri();
			}else{
				File file = new File(path);
				file =  new File(file.getAbsolutePath());
				parentPath = file.getParent();
				System.out.println(file.getParent());
				doc = Jsoup.parse(file, "UTF-8", file.getParent());
				
			}
		
			Iterator<Element> iter;
			Elements img = doc.select("img[src]");
			
			iter = img.iterator();
			while(iter.hasNext()){
				Element e = iter.next();
				String path2 = e.attr("src");
				File f2 = new File(parentPath,path2);
				if(f2.exists()){
					System.out.println("I EXIST");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public static void getLocalHTML(String path){
		Document doc = null;
		String parentPath = null;
		File file = new File(path);
		file =  new File(file.getAbsolutePath());
		parentPath = file.getParent();
		System.out.println(file.getParent());
		try {
			doc = Jsoup.parse(file, "UTF-8", file.getParent());
			Iterator<Element> iter;
			Elements img = doc.select("img[src]");	
			iter = img.iterator();
			while(iter.hasNext()){
				Element e = iter.next();
				String path2 = e.attr("src");
				File f2 = new File(parentPath,path2);
				if(f2.exists()){
					System.out.println("I EXIST");
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	
	}
	public static TreeSet<TreeMap<Attribute,Object>> getExternalHTML(String path,TreeMap<Attribute,Object> metadata){
		Document doc = null;
		String parentPath = null;
		
		TreeSet<TreeMap<Attribute,Object>> children = new TreeSet<TreeMap<Attribute,Object>>(treemapComp);
		try {
			doc = Jsoup.connect(path).get();
			parentPath = doc.baseUri();
			String js = ".js",css=".css",slash = "/",www="www",mailto="@";
			Elements title = doc.select("title");
			Elements src = doc.select("[src]");
			Elements link = doc.select("[href]");
			metadata.put(Attribute.KEYWORD, title.first().text());
			metadata.put(Attribute.PATH, path);
			Iterator<Element> iter = src.iterator();
			while(iter.hasNext()){
				String url = iter.next().attr("src");
				if(!url.contains(js)&&!url.contains(css)&&!url.contains(mailto)){
					if(url.contains(www)){
						TreeMap<Attribute,Object> attrs = new TreeMap<Attribute,Object> ();
						attrs.put(Attribute.PATH, url);
						String[] split = url.split("[/]");
						if(split.length>1){
						attrs.put(Attribute.NAME, split[split.length-1]);
						split = split[split.length-1].split("[.]");
						if(split.length>1){
						attrs.put(Attribute.TYPE, split[split.length-1]);
						}
						}
						attrs.put(Attribute.KEYWORD,split[0]);
						
						children.add(attrs);
					}else{
						TreeMap<Attribute,Object> attrs = new TreeMap<Attribute,Object> ();
						 
						if(url.contains(slash)){
							attrs.put(Attribute.PATH, doc.baseUri()+url.split("[/]")[1]);
						}else{
							attrs.put(Attribute.PATH,doc.baseUri()+url);
						}			
						String[] split = url.split("[/]");
						attrs.put(Attribute.NAME, split[split.length-1]);
						split = split[split.length-1].split("[.]");
						if(split.length>1&&split[split.length-1].length()<=10){
						attrs.put(Attribute.TYPE, split[split.length-1]);
						}
						attrs.put(Attribute.KEYWORD,split[0]);
						children.add(attrs);
					}
				}
			}
		    iter = link.iterator();
			while(iter.hasNext()){
				String url = iter.next().attr("href");
				if(!url.contains(js)&&!url.contains(css)&&!url.contains(mailto)){
					if(url.contains(www)){
						TreeMap<Attribute,Object> attrs = new TreeMap<Attribute,Object> ();
						attrs.put(Attribute.PATH, url);
						String[] split = url.split("[/]");
						if(split.length>1){
						attrs.put(Attribute.NAME, split[split.length-1]);
						split = split[split.length-1].split("[.]");
						if(split.length>1){
						attrs.put(Attribute.TYPE, split[split.length-1]);
						}
						}
						attrs.put(Attribute.KEYWORD,split[0]);					
						children.add(attrs);
					}else{
						TreeMap<Attribute,Object> attrs = new TreeMap<Attribute,Object> ();
						 
						if(url.contains(slash)){
							attrs.put(Attribute.PATH, doc.baseUri()+url);
						}else{
							attrs.put(Attribute.PATH,doc.baseUri()+"/"+url);
						}			
						String[] split = url.split("[/]");
						attrs.put(Attribute.NAME, split[split.length-1]);
						split = split[split.length-1].split("[.]");
						if(split.length>1&&split[split.length-1].length()<=10){
						attrs.put(Attribute.TYPE, split[split.length-1]);
						}
						attrs.put(Attribute.KEYWORD,split[0]);
						children.add(attrs);
					}
			
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return children;
	}
	public static boolean areFileEquals(String path1, String path2){
		File file1 = new File(path1), file2 = new File(path2);
		try {
			byte[] f1 = Files.readAllBytes(file1.toPath());
			byte[] f2 = Files.readAllBytes(file2.toPath());
			return Arrays.equals(f1, f2);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	public static void getMetadata(File file){
		TreeMap<Attribute,Object> metadata = new TreeMap<Attribute,Object>();
	}
}
